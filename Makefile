CC      = gcc
FLAGS	= -Wall -g -fpic
LFLAGS	= 

all: bin/libcatwalk.so

bin/libcatwalk.so: obj/directions.o obj/grid.o obj/indicators.o obj/route.o
	@mkdir -p $(@D)
	$(CC) -shared -o $@ $(FLAGS) $^ $(LFLAGS)

obj/%.o: src/%.c
	@mkdir -p $(@D)
	$(CC) -o $@ -c $(FLAGS) $<

obj/directions.o: src/directions.c
obj/grid.o: src/grid.c
obj/indicators.o: src/indicators.c
obj/route.o: src/route.c

.PHONY: clean mrproper install uninstall
clean:
	rm -rf obj
mrproper: clean
	rm -rf bin

install: bin/libcatwalk.so
	@mkdir -p /usr/local/include/catwalk/
	install -p include/*.h /usr/local/include/catwalk/
	@mkdir -p /usr/local/lib
	install -p bin/libcatwalk.so /usr/local/lib/
	# configuration of links
	echo "/usr/local/lib/libcatwalk.so" > /etc/ld.so.conf.d/libcatwalk.conf
	ldconfig /usr/local/lib/libcatwalk.so

uninstall:
	-rm -rf /usr/local/include/catwalk
	-rm -f /usr/local/lib/libcatwalk.so
	# reconfiguration of links
	ldconfig /usr/local/lib/libcatwalk.so
