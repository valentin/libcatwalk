/* CATWALK - Test your logic
   Copyright (C) 2021 Valentin Moguerou
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE. See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>  */

#ifndef GRID_H_INCLUDED
#define GRID_H_INCLUDED

#include "indicators.h"
#include "route.h"

typedef struct grid grid;
struct grid
{
    int width;
    int start[2], end[2];
    indicators *hints;
    route *player_route;
};

grid *init_grid(int width);
void random_grid(grid *gd, int seed);
void random_start(grid *gd);
void refresh_grid(grid *gd);
void init_player_route(grid *gd);
void reset_player_route(grid *gd);
void delete_grid(grid *gd);

int verify(grid *gd);

#endif /* GRID_H_INCLUDED */