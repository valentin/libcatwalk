/* CATWALK - Test your logic
   Copyright (C) 2021 Valentin Moguerou
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE. See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>  */

#ifndef DIRECTIONS_H_INCLUDED
#define DIRECTIONS_H_INCLUDED

#include "route.h"

typedef struct
{
    route *rt;       // a directions object is linked to a route
    int count;       // number of possible moves
    int cells[4][2]; // four directions : left, right, up, down
} directions;

    /* Inits the directions object. */
directions *init_directions(route *rt);

    /* Evaluates which cells are free.
       Returns the number of moves */
int evaluate_directions(directions *dirs);

    /* Moves randomly in rt */
void push_choose(route *rt, directions *dirs);

    /* Destroys the directions object by freeing the memory. */
void delete_directions(directions *dirs);

#endif /* DIRECTIONS_H_INCLUDED */