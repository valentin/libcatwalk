/* CATWALK - Test your logic
   Copyright (C) 2021 Valentin Moguerou
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE. See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>  */

#ifndef INDICATORS_H_INCLUDED
#define INDICATORS_H_INCLUDED

#include "route.h"

typedef struct
{
    int *x;
    int *y;
} indicators;

indicators *init_indicators(int width);
void setup_indicators(indicators *in, route *rt);
void delete_indicators(indicators *hints);

#endif /* INDICATORS_H_INCLUDED */