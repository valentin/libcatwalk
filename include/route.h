/* CATWALK - Test your logic
   Copyright (C) 2021 Valentin Moguerou
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE. See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>  */

#ifndef ROUTE_H_INCLUDED
#define ROUTE_H_INCLUDED

typedef struct element element;
struct element
{
    int x, y;
    element *previous, *next;
};

typedef struct
{
    int width;
    element *first, *last;
} route;

/*

          -- second --
-- second -- first  -- second
          -- second --

*/

int is_left(element *first, element *second);
int is_right(element *first, element *second);
int is_up(element *first, element *second);
int is_down(element *first, element *second);


/*
    Return type: int
    * 0: success
    * 1: cannot move because of the grid border
    * 2: cannot move: already went here
*/
int move_left(route *rt);
int move_right(route *rt);
int move_up(route *rt);
int move_down(route *rt);

route *init_route(int width, int startx, int starty);
void delete_element(element *el);
void delete_route(route *rt);

void push_back(route *rt, int x, int y);
void pop_back(route *rt);

element *find_coordinates(route *rt, int x, int y);

int is_free(route *rt, int x, int y);
void generate(route *rt);

#endif /* ROUTE_H_INCLUDED */