# Bibliothèque libcatwalk

La bibliothèque du jeu Catwalk, servant de base pour que ses différentes interfaces puissent fonctionner.

## Licence

Copyright Valentin Moguérou, 2021.

Ce programme est un logiciel libre. Vous pouvez voir chaque fichier pour connaître vos droits. Une copie de la License Publique Générale de GNU est disponible à la racine du code source dans le fichier [LICENSE](https://git.kaz.bzh/valentin/libcatwalk/src/branch/master/LICENSE).

## Remerciements

Merci à [Arnaud Gazagnes](mailto:arnaud.gazagnes@ac-lyon.fr) pour la compilation d'une multitude de jeux dans un document : [Grilles logiques, 69 jeux de grilles](http://math.univ-lyon1.fr/irem/IMG/pdf/GrillesLogiques.pdf).

Merci à [PuzzlePicnic](http://puzzlepicnic.com) pour avoir hébergé une [description de ce jeu](http://puzzlepicnic.com/genre?catwalk), et de ce fait permis à Arnaud Gazagnes de le recenser.

## Comment s'organise-t-elle ?

Quatre couples de fichiers :
+ `grid.h` et `grid.c`, qui sert de base, qui définit la structure de la grille ;
+ `route.h` et `route.c`, qui définit la structure du parcours du chat, qui sert à la fois pour la génération de la grille, et pour le parcours joué ;
+ `indicators.h` et `indicators.c`, qui définit la structure des nombres affichés autour de la grille ;
+ `directions.h` et `directions.c`, qui définit la structure des directions que peut prendre le chat après une case.

Ainsi, les fichiers sont installés dans :
+ `/usr/local/lib/libcatwalk.so`
+ `/usr/local/include/catwalk/*.h`

## Instructions d'installation

1. En premier lieu, il faut cloner le dépôt et y accéder:
```
$ git clone https://git.kaz.bzh/valentin/libcatwalk.git
$ cd libcatwalk
```
2. Ensuite, il faut compiler la bibliothèque : 
```
$ make
```
3. Enfin, il faut installer la bibliothèque :
```
$ sudo make install
```
ou
```
# make install
```

## Désinstallation

```
$ sudo make uninstall
```
ou
```
# make uninstall
```

## Règles du jeu

Le but du jeu est de dessiner un chemin de chaque chat à son propre bol de lait.

Les nombres à gauche et au-dessous de la grille indiquent combien de cases sont traversées par ce chemin, chats et bols compris.

Les chats se promènent seulement horizontalement et verticalement et aucune case n’est visitée plus d’une fois.

Source : Arnaud Gazagnes (arnaud.gazagnes@ac-lyon.fr), dans [Grille logiques, 69 jeux de grilles](http://math.univ-lyon1.fr/irem/IMG/pdf/GrillesLogiques.pdf).

## Exemple

Cet exemple a été réalisé grâce à [catwalk-cli](https://git.kaz.bzh/valentin/catwalk-cli/).

```
$ catwalk-cli
                                        
           1       1       2       0    
                                        
       +-------+-------+-------+-------+
       | /\_/\ |       |       |       |
   3   |( o.o )|       |       |       |
       | > ^ < |       |       |       |
       +-------+-------+-------+-------+
       |       |       |  ==   |       |
   1   |       |       | /mi\  |       |
       |       |       | \lk/  |       |
       +-------+-------+-------+-------+
       |       |       |       |       |
   0   |       |       |       |       |
       |       |       |       |       |
       +-------+-------+-------+-------+
       |       |       |       |       |
   0   |       |       |       |       |
       |       |       |       |       |
       +-------+-------+-------+-------+
```

## Fonctionnalités
+ Création d'un objet `struct grid` possédant:
  - une largeur;
  - une paire de coordonnées de la case de départ;
  - une paire de coordonnées de la case d'arrivée;
  - un pointeur vers des "indicateurs", des nombres qui servent de contraintes au joueur;
  - un pointeur vers le parcours du joueur, quand il existe (sinon le pointeur est nul).
+ Il existe des fonctions pour générer une grille, pour régénérer une grille, et pour vérifier une grille jouée.
+ Création d'un objet `struct indicators`, représentant les nombres aux côtés de la grille. Il possède deux pointeurs vers des tableaux d'entiers, un pour chaque axe, horizontal et vertical.
+ Création d'un objet `struct route`, représentant un parcours. Cette structure est utilisée à la fois pour la génération de la grille, et pour les mouvements du joueur. Il s'agit en réalité d'une liste doublement chaînée.
+ Cet objet dépend d'un autre objet `struct element` qui contient les coordonnées d'une case, et qui contient également les pointeurs de l'élément précédent et de l'élément suivant, dans le contexte de la liste doublement chaînée.
+ Création d'un objet `struct directions`, un objet qui sert à identifier les cases sur lesquelles le chat peut aller. Sur l'exemple ci-dessus, le chat se situe sur la case `(0, 0)`, et peut se déplacer sur les cases de coordonnées `(0, 1)` et `(1, 0)`. Ici, la variable `count` de l'objet est égale à 2 car il y a deux possibilités.

Cette bibliothèque est utilisée, pour l'heure, par le seul projet [catwalk-cli](https://git.kaz.bzh/valentin/catwalk-cli/).