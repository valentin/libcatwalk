/* CATWALK - Test your logic
   Copyright (C) 2021 Valentin Moguerou
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE. See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>  */

#include <stdlib.h>
#include "../include/directions.h"
#include "../include/route.h"

#define RANDINT(MAX) (rand() % MAX)

directions *init_directions(route *rt)
{
    directions *dirs = malloc(sizeof(*dirs));
    if (dirs==NULL)
    {
        exit(EXIT_FAILURE);
    }

    dirs->rt = rt;
    dirs->count = 0;

    return dirs;
}

int evaluate_directions(directions *dirs)
{
    int width = dirs->rt->width;
    element *last = dirs->rt->last;

    int count = 0;
    if (last->x != 0 && is_free(dirs->rt, last->x-1, last->y))
    {
        dirs->cells[count][0] = last->x-1;
        dirs->cells[count][1] = last->y;
        count++;
    }
    if (last->x != (width-1) && is_free(dirs->rt, last->x+1, last->y))
    {
        dirs->cells[count][0] = last->x+1;
        dirs->cells[count][1] = last->y;
        count++;
    }
    if (last->y != 0 && is_free(dirs->rt, last->x, last->y-1))
    {
        dirs->cells[count][0] = last->x;
        dirs->cells[count][1] = last->y-1;
        count++;
    }
    if (last->y != (width-1) && is_free(dirs->rt, last->x, last->y+1))
    {
        dirs->cells[count][0] = last->x;
        dirs->cells[count][1] = last->y+1;
        count++;
    }
    dirs->count = count;
    return count;
}

void push_choose(route *rt, directions *dirs)
{
    int nb = RANDINT(dirs->count);
    push_back(rt, dirs->cells[nb][0], dirs->cells[nb][1]);
}

void delete_directions(directions *dirs)
{
    free(dirs);
}