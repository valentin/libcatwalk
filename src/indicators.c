/* CATWALK - Test your logic
   Copyright (C) 2021 Valentin Moguerou
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE. See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>  */

#include <stdlib.h>
#include "../include/indicators.h"
#include "../include/route.h"

indicators *init_indicators(int width)
{
    indicators *in = malloc(sizeof(*in));
    if (in == NULL)
    {
        exit(EXIT_FAILURE);
    }

    in->x = calloc(width, sizeof(int));
    in->y = calloc(width, sizeof(int));
    if (in->x==NULL || in->y==NULL)
    {
        exit(EXIT_FAILURE);
    }

    return in;
}

void setup_indicators(indicators *in, route *rt)
{
    for (int i=0; i<rt->width; i++)
    {
        in->x[i] = 0;
        in->y[i] = 0;
    }
    for (element *el=rt->first; el; el=el->next)
    {
        in->x[el->x]++;
        in->y[el->y]++;
    }
}

void delete_indicators(indicators *hints)
{
    free(hints->x);
    free(hints->y);
    free(hints);
}
