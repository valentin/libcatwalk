/* CATWALK - Test your logic
   Copyright (C) 2021 Valentin Moguerou
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE. See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>  */

#include <stdlib.h>
#include "../include/grid.h"
#include "../include/indicators.h"
#include "../include/route.h"

#define RANDINT(MAX) (rand() % MAX)

grid *init_grid(int width)
{
    grid *gd = malloc(sizeof(grid));
    if (gd == NULL)
    {
        exit(EXIT_FAILURE);
    }

    gd->width = width;
    gd->hints = init_indicators(width);
    gd->player_route = NULL;

    return gd;
}

void random_grid(grid *gd, int seed)
{
    srand(seed);
    random_start(gd);
    refresh_grid(gd);
}

void random_start(grid *gd)
{
    gd->start[0] = RANDINT(gd->width);
    gd->start[1] = RANDINT(gd->width);
}

void refresh_grid(grid *gd)
{
    route *rt = init_route(gd->width, gd->start[0], gd->start[1]);

    generate(rt);
    setup_indicators(gd->hints, rt);
    
    element *last = rt->last;
    gd->end[0] = last->x;
    gd->end[1] = last->y;
    
    delete_route(rt);
}

void init_player_route(grid *gd)
{
    route *player_rt = malloc(sizeof(*player_rt));
    if (player_rt == NULL)
    {
        exit(EXIT_FAILURE);
    }

    player_rt = init_route(gd->width, gd->start[0], gd->start[1]);

    gd->player_route = player_rt;
}

void reset_player_route(grid *gd)
{
    delete_route(gd->player_route);
    init_player_route(gd);
}

void delete_grid(grid *gd)
{
    delete_indicators(gd->hints);
    if (gd->player_route)
    {
        delete_route(gd->player_route);
    }
    free(gd);
}

int verify(grid *gd)
{
    indicators *verif_in = init_indicators(gd->width);
    setup_indicators(verif_in, gd->player_route);

    for (int i=0; i<gd->width; i++)
    {
        if ((verif_in->x[i] != gd->hints->x[i]) || (verif_in->y[i] != gd->hints->y[i]))
        {
            return 0;
        }
    }
    return 1;
}