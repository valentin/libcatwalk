/* CATWALK - Test your logic
   Copyright (C) 2021 Valentin Moguerou
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULIAR PURPOSE. See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>  */

#include <stdlib.h>
#include "../include/directions.h"
#include "../include/route.h"

#define RANDINT(MIN, MAX) ((rand()%(MAX-MIN)) + MIN)

int is_left(element *first, element *second)
{
    return first->x-1 == second->x;
}

int is_right(element *first, element *second)
{
    return first->x+1 == second->x;
}

int is_up(element *first, element *second)
{
    return first->y-1 == second->y;
}

int is_down(element *first, element *second)
{
    return first->y+1 == second->y;
}

int move_left(route *rt)
{
    element *last = rt->last;
    if (last->x == 0)
    {
        return 1;
    }
    else if (!is_free(rt, last->x-1, last->y))
    {
        return 2;
    }
    else
    {
        push_back(rt, last->x-1, last->y);
        return 0;
    }
}

int move_right(route *rt)
{
    element *last = rt->last;
    if (last->x == rt->width-1)
    {
        return 1;
    }
    else if (!is_free(rt, last->x+1, last->y))
    {
        return 2;
    }
    else
    {
        push_back(rt, last->x+1, last->y);
        return 0;
    }
}

int move_up(route *rt)
{
    element *last = rt->last;
    if (last->y == 0)
    {
        return 1;
    }
    else if (!is_free(rt, last->x, last->y-1))
    {
        return 2;
    }
    else
    {
        push_back(rt, last->x, last->y-1);
        return 0;
    }
}

int move_down(route *rt)
{
    element *last = rt->last;
    if (last->y == (rt->width-1))
    {
        return 1;
    }
    else if (!is_free(rt, last->x, last->y+1))
    {
        return 2;
    }
    else
    {
        push_back(rt, last->x, last->y+1);
        return 0;
    }
}

route *init_route(int width, int startx, int starty)
{
    route *rt = malloc(sizeof(*rt));
    element *el = malloc(sizeof(*el));

    if (rt == NULL || el == NULL)
    {
        exit(EXIT_FAILURE); // memory allocation failed
    }

    el->previous = NULL;
    el->next = NULL;

    el->x = startx;
    el->y = starty;

    rt->width = width;
    rt->first = el;
    rt->last = el;

    return rt;
}

void delete_element(element *el)
{
    if (el->next)
    {
        delete_element(el->next);
    }
    free(el);
}

void delete_route(route *rt)
{
    delete_element(rt->first);
    free(rt);
}

void push_back(route *rt, int x, int y)
{
    element *new_el = malloc(sizeof(*new_el));
    if (new_el == NULL)
    {
        exit(EXIT_FAILURE);
    }

    new_el->x = x;
    new_el->y = y;
    new_el->previous = rt->last;
    new_el->next = NULL;

    rt->last->next = new_el;
    rt->last = new_el;
}

void pop_back(route *rt)
{
    element *last = rt->last;
    rt->last = rt->last->previous;
    rt->last->next = NULL;
    free(last);
}

element *find_coordinates(route *rt, int x, int y)
{
    for (element *el=rt->first; el; el=el->next)
    {
        if (el->x == x && el->y == y)
        {
            return el;
        }
    }
    return NULL;
}

int is_free(route *rt, int x, int y)
{
    for (element *el=rt->first; el; el=el->next)
    {
        if (x==el->x && y==el->y)
        {
            return 0;
        }
    }
    return 1;
}

void generate(route *rt)
{
    directions *dirs = init_directions(rt);

    for (int i=0; i < RANDINT(1, rt->width*rt->width); i++)
    {
        if (!evaluate_directions(dirs))
        {
            break;
        }
        push_choose(rt, dirs);
    }
}
